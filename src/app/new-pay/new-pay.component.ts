import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

export interface TransactionType {
  code: string,
  displayText: string
}

export interface Transaction{
  type: string,
  amount: number,
  comment: string
}

@Component({
  selector: 'app-new-pay',
  templateUrl: './new-pay.component.html',
  styleUrls: ['./new-pay.component.css']
})
export class NewPayComponent implements OnInit {
  public form: FormGroup;

  selectedTransactionType = 'spend';
  transactionTypes: TransactionType[] = [
    {code: 'spend', displayText:'Spend'},
    {code: 'receive', displayText: 'Receive'}
  ]

  constructor(private fb: FormBuilder) {
    //super();
  }

  ngOnInit() {
    this.buildForm();
  }

  buildForm(){
    this.form = this.fb.group({
      transactionType: [null, Validators.compose([Validators.required])],
      amount: [null, Validators.compose([Validators.required])],
      comment: [null]
    })
  }

}
